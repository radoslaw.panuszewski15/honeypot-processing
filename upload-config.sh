#!/bin/bash

# helper script for updating the Docker Swarm configuration while the machines are already running

function upload-file() {
    vagrant scp "$2" "$1:~/$2"
}

upload-file normal docker-compose.yml
upload-file normal config-http.toml
upload-file normal config-smtp.toml
upload-file normal config-ssh.toml