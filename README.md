# Honeypot Processing
The project was realized during the PSD course on Warsaw University of Technology.

## Prerequisites
In order to start the virtual machines you need to have:
  * [VirtualBox](https://www.virtualbox.org/wiki/Downloads) installed
    ```bash
    # for Ubuntu 19.10 / 20.04 / 20.10 / 21.04
    curl https://download.virtualbox.org/virtualbox/6.1.22/virtualbox-6.1_6.1.22-144080~Ubuntu~eoan_amd64.deb --output virtualbox.deb
    sudo dkpg -i ./virtualbox.deb
    ```
  * [Vagrant](https://www.vagrantup.com/downloads) installed
    ```bash
    # for Ubuntu (or any other distro with snapd installed)
    sudo snap install vagrant --classic
    ```

## Network setup

Clone the repo and enter the project directory
```bash
git clone https://gitlab.com/radoslaw.panuszewski15/honeypot-processing.git
cd honeypot-processing
```

Configure and start the virtual machines
```bash
vagrant up
```

Test if everything works
```bash
vagrant ssh attacker # login to attacker machine
ping 192.168.248.2 # try to ping the victim machine
traceroute 192.168.248.2 # just make sure that the traffic goes through the router
```

If you want to show the Kali Linux GUI then open the VirtualBox app, select attacker machine and click `Show`.
Login with the credentials `vagrant/vagrant` and wait for desktop to appear. Now you can maximize the 
VM window to have a bigger view (if you maximized it on login screen then simply un-maximize it and maximize again).
![img.png](doc/virtualbox-kali.png)

Other useful commands:
```bash
# the name parameter must be a space-separated list of following: [router, attacker, victim, normal]
# if [name] appears in square brackets then it's optional

vagrant ssh name # start SSH session 
vagrant reload [name] # restart the virtual machine(s) and apply changes in Vagrantfile
vagrant reload [name] --provision # same as above but also run the provisioning script
vagrant suspend [name] # save the virtual machine(s) state and suspend it
vagrant destroy [name] # completely destroy the virtual machine(s)
```

## MongoDB

The Flink job will save alarms to the MongoDB database which should be available on localhost:27017. You can start it
using docker-compose:
```bash
cd docker/mongo
docker-compose up
```

The default credentials for the MongoDB instance you just started are `admin/admin`

## Deploy Flink job

First make sure you have Flink downloaded with version 1.13.1 for Scala 2.11, if not - you can do it [here](https://www.apache.org/dyn/closer.lua/flink/flink-1.13.1/flink-1.13.1-bin-scala_2.11.tgz).

Start it:
```bash
cd flink-1.13.1/bin
./start-cluster.sh
```

Now go back to the Honeypot Processing project root directory and execute:
```
./gradlew copyExampleFiles
```

It will copy the `gradle.properties.example` file and rename it to `gradle.properties` in project directory. Modify its 
contents to match your Flink installation directory (you have to do it only once):
```
flink.home=/path/to/your/flink
```
From now on, you can edit the `gradle.properties` file and do whatever you want with it ;) It is added to `.gitignore`
so your local path to flink won't be pushed into repository.

Now you're ready to compile and deploy the job to Flink. It can be done via executing following task:
```
./gradlew flinkRun
```

You can see logs from the deployed job under `localhost:8081` -> `Task Managers` -> (the only manager there) -> `Stdout`