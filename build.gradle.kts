plugins {
    id("com.github.johnrengelman.shadow") version "7.0.0"
    java
    idea
}

group = "com.pw"
version = "1.0-SNAPSHOT"

val flinkVersion = "1.13.1"
val scalaVersion = "2.11"
val mongoVersion = "3.12.8"
val log4jVersion = "2.12.1"

repositories {
    mavenCentral()
}

dependencies {

    // Flink
    implementation("org.apache.flink", "flink-walkthrough-common_$scalaVersion", flinkVersion)
    implementation("org.apache.flink", "flink-streaming-java_$scalaVersion", flinkVersion)
    implementation("org.apache.flink", "flink-clients_$scalaVersion", flinkVersion)
    implementation("org.apache.flink", "flink-connector-kafka_$scalaVersion", flinkVersion)

    // MongoDB
    implementation("org.mongodb", "mongo-java-driver", mongoVersion)

    // Logging
    implementation("org.apache.logging.log4j", "log4j-slf4j-impl", log4jVersion)
    implementation("org.apache.logging.log4j", "log4j-api", log4jVersion)
    implementation("org.apache.logging.log4j", "log4j-core", log4jVersion)
}

tasks {

    jar {
        manifest {
            attributes["Main-Class"] = "com.pw.hp.AttackDetectionJob"
        }
    }

    register<Copy>("copyExampleFiles") {
        group ="build setup"
        description = "Copy example files and remove the *.example suffix"
        from("gradle.properties.example")
        into(".")
        rename { it.replace(".example", "") }
    }

    register<Exec>("flinkRun") {
        group = "application"
        description = "Run Job on Flink"
        dependsOn("shadowJar")
        commandLine("${project.properties["flink.home"]}/bin/flink", "run", "-d", "build/libs/honeypot-processing-1.0-SNAPSHOT-all.jar")

        doFirst {
            exec {
                commandLine("${project.properties["flink.home"]}/bin/stop-cluster.sh")
            }
            exec {
                commandLine("${project.properties["flink.home"]}/bin/start-cluster.sh")
            }
        }
    }
}