package com.pw.hp;

import java.time.Instant;
import java.util.StringJoiner;

import static com.pw.hp.AttackDetectionJob.ALARM_THRESHOLD;
import static java.lang.String.format;

public class Alarm {

    private final String title;
    private final String description;
    private final String category;
    private final long trafficIntensity;
    private final long timestamp;

    private Alarm(String title, String description, String category, long trafficIntensity, long timestamp) {
        this.title = title;
        this.description = description;
        this.timestamp = timestamp;
        this.category = category;
        this.trafficIntensity = trafficIntensity;
    }

    public static Alarm of(String category, long trafficIntensity, Instant timestamp) {
        return new Alarm(
            format("[%s] Detected alarm in service %s", timestamp, category),
            format("The incoming traffic has exceeded security threshold [%s/%s], your network may be under attack!", trafficIntensity, ALARM_THRESHOLD),
            category,
            trafficIntensity,
            timestamp.toEpochMilli());
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getCategory() {
        return category;
    }

    public long getTrafficIntensity() {
        return trafficIntensity;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Alarm.class.getSimpleName() + "[", "]")
            .add("title='" + title + "'")
            .add("description='" + description + "'")
            .add("category='" + category + "'")
            .add("trafficIntensity=" + trafficIntensity)
            .add("timestamp=" + timestamp)
            .toString();
    }
}
