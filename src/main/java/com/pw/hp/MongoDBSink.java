package com.pw.hp;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.core.type.TypeReference;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.bson.Document;

import java.util.Map;

public class MongoDBSink<T> extends RichSinkFunction<T> {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private MongoCollection<Document> collection;

    @Override
    public void invoke(T value, Context context) throws JsonProcessingException {

        if (collection != null) {

            if (value instanceof ObjectNode) {
                saveObjectNode((ObjectNode) value);
            }
            else {
                saveObject(value);
            }
        }
    }

    private void saveObjectNode(ObjectNode objectNode) throws JsonProcessingException {

        var json = objectNode.get("value");
        var fixedJson = MAPPER.readTree(json.toString().replace(".", "-"));
        saveObject(fixedJson);
    }

    private void saveObject(Object object) {

        var map = MAPPER.convertValue(object, new TypeReference<Map<String, Object>>() {});
        collection.insertOne(new Document(map));
    }

    @Override
    public void open(Configuration config) {

        collection = MongoClients.create("mongodb://admin:admin@localhost:27017")
            .getDatabase("hp-db")
            .getCollection("alarms");
    }
}
