package com.pw.hp;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.util.serialization.JSONKeyValueDeserializationSchema;
import org.apache.flink.util.Collector;

import java.time.Instant;
import java.util.Map;
import java.util.Properties;

import static java.util.concurrent.TimeUnit.SECONDS;


public class AttackDetectionJob {

    public static final int ALARM_THRESHOLD = 10;

    private static final String SSH_EVENTS_TOPIC = "honeytrap-events-ssh";
    private static final String HTTP_EVENTS_TOPIC = "honeytrap-events-http";
    private static final String SMTP_EVENTS_TOPIC = "honeytrap-events-smtp";

    public static void main(String[] args) throws Exception {

        var env = StreamExecutionEnvironment.getExecutionEnvironment();

        var sshEvents = env.addSource(createKafkaConsumer(SSH_EVENTS_TOPIC));
        var httpEvents = env.addSource(createKafkaConsumer(HTTP_EVENTS_TOPIC));
        var smtpEvents = env.addSource(createKafkaConsumer(SMTP_EVENTS_TOPIC));

        var events = sshEvents
            .union(httpEvents)
            .union(smtpEvents)
            .map(Event::of)
            .filter(event -> !event.getCategory().equals("heartbeat"))
            .keyBy(Event::getCategory)
            .window(TumblingProcessingTimeWindows.of(Time.of(1, SECONDS)))
            .aggregate(new EventCategoryCounter())
            .flatMap((Map<String, Long> eventOccurrences, Collector<Alarm> collector) -> eventOccurrences.entrySet().stream()
                .filter(it -> it.getValue() > ALARM_THRESHOLD)
                .map(it -> Alarm.of(it.getKey(), it.getValue(), Instant.now()))
                .forEach(collector::collect))
            .returns(Alarm.class);

        events.addSink(new MongoDBSink<>());
        events.print();

        env.execute("Attack Detection");
    }

    private static FlinkKafkaConsumer<ObjectNode> createKafkaConsumer(String topic) {

        var properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        return new FlinkKafkaConsumer<>(topic, new JSONKeyValueDeserializationSchema(true), properties);
    }

}