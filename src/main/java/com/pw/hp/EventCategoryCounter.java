package com.pw.hp;

import org.apache.flink.api.common.functions.AggregateFunction;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

@SuppressWarnings("Convert2MethodRef")
class EventCategoryCounter implements AggregateFunction<Event, Map<String, Long>, Map<String, Long>> {

    @Override
    public Map<String, Long> createAccumulator() {
        return new HashMap<>();
    }

    @Override
    public Map<String, Long> add(Event value, Map<String, Long> accumulator) {

        var acc = new HashMap<>(accumulator);
        acc.put(value.getCategory(), acc.getOrDefault(value.getCategory(), 0L) + 1);
        return acc;
    }

    @Override
    public Map<String, Long> getResult(Map<String, Long> accumulator) {

        return accumulator;
    }

    @Override
    public Map<String, Long> merge(Map<String, Long> first, Map<String, Long> second) {

        return Stream.concat(first.entrySet().stream(), second.entrySet().stream())
            .collect(toMap(
                it -> it.getKey(),
                it -> it.getValue(),
                (a, b) -> a + b));
    }
}
