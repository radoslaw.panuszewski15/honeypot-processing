package com.pw.hp;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.Serializable;
import java.util.Optional;
import java.util.StringJoiner;

public class Event implements Serializable {

    private final ObjectNode node;

    private Event(ObjectNode node) {
        this.node = node;
    }

    public static Event of(ObjectNode node) {
        return new Event(node);
    }

    public String getCategory() {

        return Optional.ofNullable(node.get("value"))
            .map(value -> value.get("category"))
            .map(JsonNode::asText)
            .orElse("");
    }

    public String getDestinationIp() {

        return Optional.ofNullable(node.get("value"))
            .map(value -> value.get("destination-ip"))
            .map(JsonNode::asText)
            .orElse("");
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Event.class.getSimpleName() + "[", "]")
            .add("node=" + node)
            .toString();
    }
}
