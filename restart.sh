#!/bin/bash

# helper script for restarting locally deployed services (which in target are going to be deployed on Docker Swarm)

docker-compose down
docker-compose up $1