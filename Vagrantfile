Vagrant.configure("2") do |config|

    config.vagrant.plugins = ["vagrant-reload", "vagrant-scp"]

    config.vm.define "attacker" do |attacker|
        attacker.vm.box = "kalilinux/rolling"
        attacker.vm.hostname = "attacker"

        attacker.vm.network "private_network", ip: "192.168.232.2"

        attacker.vm.provision "shell",
          inline: <<-SHELL
            cat <<EOF >>/etc/hosts
            192.168.232.2   attacker
            192.168.248.2   victim
            192.168.248.3   normal
            192.168.232.10  router
EOF
          SHELL
        attacker.vm.provision "shell",
          run: "always",
          inline: <<-SHELL
            ip route add 192.168.248.0/24 via 192.168.232.10
          SHELL
        attacker.vm.provision :reload

        attacker.vm.provider "virtualbox" do |v|
            v.gui = false
        end
    end

    config.vm.define "normal" do |normal|
        normal.vm.box = "hashicorp/bionic64"
        normal.vm.hostname = "normal"

        normal.vm.network "private_network", ip: "192.168.248.3"
        normal.vm.network "forwarded_port", host: 9092, guest: 9092

        normal.vm.provision "file", source: "./docker-compose.yml", destination: "~/docker-compose.yml"
        normal.vm.provision "file", source: "./config-ssh.toml", destination: "~/config-ssh.toml"
        normal.vm.provision "file", source: "./config-smtp.toml", destination: "~/config-smtp.toml"
        normal.vm.provision "file", source: "./config-http.toml", destination: "~/config-http.toml"
        normal.vm.provision "shell",
          inline: <<-SHELL
          apt-get update
          apt-get install -y traceroute
          cat <<EOF >>/etc/hosts
          192.168.232.2   attacker
          192.168.248.2   victim
          192.168.248.3   normal
          192.168.248.10  router
EOF
            apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
            echo \
              "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
              $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
            apt-get update
            apt-get install -y docker-ce docker-ce-cli containerd.io
            usermod -aG docker vagrant
            curl \
              -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" \
              -o /usr/local/bin/docker-compose 2>/dev/null
            docker swarm init --advertise-addr 192.168.248.3
            docker swarm join-token -q worker > /home/vagrant/docker-swarm-join-token
            docker stack deploy --compose-file=/home/vagrant/docker-compose.yml hp
          SHELL
        normal.vm.provision "shell",
          run: "always",
          inline: <<-SHELL
            ip route add 192.168.232.0/24 via 192.168.248.10
          SHELL
        normal.vm.provision :reload
    end

    config.vm.define "victim" do |victim|
        victim.vm.box = "hashicorp/bionic64"
        victim.vm.hostname = "victim"

        victim.vm.network "private_network", ip: "192.168.248.2"

        victim.vm.provision "file", source: "./config-ssh.toml", destination: "~/config-ssh.toml"
        victim.vm.provision "file", source: "./config-smtp.toml", destination: "~/config-smtp.toml"
        victim.vm.provision "file", source: "./config-http.toml", destination: "~/config-http.toml"
        victim.vm.provision "shell",
          inline: <<-SHELL
            apt-get update
            apt-get install -y traceroute
            cat <<EOF >>/etc/hosts
            192.168.232.2   attacker
            192.168.248.2   victim
            192.168.248.3   normal
            192.168.248.10  router
EOF
            apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
            echo \
              "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
              $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
            apt-get update
            apt-get install -y docker-ce docker-ce-cli containerd.io
            usermod -aG docker vagrant
            curl \
              -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" \
              -o /usr/local/bin/docker-compose 2>/dev/null
            apt-get install -y sshpass
            sshpass -p "vagrant" scp -o StrictHostKeyChecking=no vagrant@normal:~/docker-swarm-join-token .
            TOKEN=$(cat docker-swarm-join-token)
            docker swarm join --token $TOKEN normal:2377
          SHELL
        victim.vm.provision "shell",
          run: "always",
          inline: <<-SHELL
            ip route add 192.168.232.0/24 via 192.168.248.10
          SHELL
        victim.vm.provision :reload
    end

    config.vm.define "router" do |router|
        router.vm.box = "hashicorp/bionic64"
        router.vm.hostname = "router"

        router.vm.network "private_network", ip: "192.168.232.10"
        router.vm.network "private_network", ip: "192.168.248.10"

        router.vm.provision "shell",
          inline: <<-SHELL
            apt-get install -y traceroute
            cat <<EOF >>/etc/hosts
            192.168.232.2   attacker
            192.168.248.2   victim
            192.168.248.3   normal
EOF
            echo -e "\nnet.ipv4.ip_forward=1" >> /etc/sysctl.conf
            sysctl -p
          SHELL
        router.vm.provision :reload
    end
end
